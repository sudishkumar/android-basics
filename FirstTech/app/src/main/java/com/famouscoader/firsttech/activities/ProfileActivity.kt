package com.famouscoader.firsttech.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import com.famouscoader.firsttech.R

class ProfileActivity: AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)

       var fname=   findViewById<TextView>(R.id.fName_textView)
       val lname=   findViewById<TextView>(R.id.lName_textView)
        val fName = intent.extras?.get("first_name")
        val lName = intent.extras?.get("last_name")
       // fname=fName.text
         fname.setText(fName.toString())
        lname.text = lName.toString()

    }
}