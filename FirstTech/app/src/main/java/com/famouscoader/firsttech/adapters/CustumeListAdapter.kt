package com.famouscoader.firsttech.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.famouscoader.firsttech.R
import com.famouscoader.firsttech.models.CustomListModel


/*
        val customDataList = ArrayList<CustomListModel>()
        customDataList.add(CustomListModel(name = "Sk", imageURL = "", email = "Sk@gmail.com")) 0
        customDataList.add(CustomListModel(name = "Sks", imageURL = "", email = "Sks@gmail.com")) 1
        customDataList.add(CustomListModel(name = "Sk1", imageURL = "", email = "Sk1@gmail.com")) 2
        customDataList.add(CustomListModel(name = "Sk2", imageURL = "", email = "Sk2@gmail.com")) 3
        customDataList.add(CustomListModel(name = "Sk3", imageURL = "", email = "Sk3@gmail.com")) 4
        customDataList.add(CustomListModel(name = "Sk4", imageURL = "", email = "Sk4@gmail.com")) 5
        customDataList.add(CustomListModel(name = "Sk5", imageURL = "", email = "Sk5@gmail.com")) 6
 */


// we have created a class ( CustomAdaptor.kt) and it passing data as array and array data type is CustomListModel.kt
// and we are also passing context in ( CustomAdaptor.kt) which is our class
// dataList and context are variable names
// BaseAdapter() is main class from in where we are getting some functions
// Like getCount, getItem, getItemId, and getView

class CustomAdaptor(val  dataList: ArrayList<CustomListModel>, val context: Context) :BaseAdapter(){


    override fun getCount(): Int {
        // returning size of list. it mean how many item will be
        return  dataList.size
    }

    override fun getItem(position: Int): Any {
        // it will get data according to position
        // Like at 0 index our data is
        // customDataList.add(CustomListModel(name = "Sk", imageURL = "", email = "Sk@gmail.com"))
        return dataList[position]
    }

    override fun getItemId(position: Int): Long {
        // it will convert int position to Long because it's return type data is Long
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        // calling / adding (custom_layout_item.xml ) layout of using LayoutInflater
        val rowView = LayoutInflater.from(context).inflate(R.layout.custom_layout_item, parent, false)

        // finding views from rowView variable in which we called our custom_layout_item.xml layout
        val  name = rowView.findViewById<TextView>(R.id.name)
        val  email = rowView.findViewById<TextView>(R.id.email)
        val imageView = rowView.findViewById<ImageView>(R.id.image)

        // setting text on TextView like this ( name.text = dataList[position].name )
        // we have out textView in name and email
        name.text = dataList[position].name
        email.text = dataList[position].email

        // show images using glide library
        Glide.with(context).load(dataList[position].imageURL).error(R.drawable.circular_shape).into(imageView)

        // returning view with data ( custom_layout_item layout and data)
        // Each data will take one view
        return rowView
    }
}