package com.famouscoader.firsttech.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.ListView
import com.famouscoader.firsttech.objects.Global.studentDetailsArray
import com.famouscoader.firsttech.R

class StudentListActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_student_list)


        val studentName = intent.extras?.get("name")
        val studentEmail = intent.extras?.get("email")

        studentDetailsArray.add(studentName.toString() + " " + studentEmail.toString())

        val studentArrayAdapter = ArrayAdapter(this, R.layout.student_list_item, studentDetailsArray)

        val studentListView = findViewById<ListView>(R.id.student_listView)

        studentListView.adapter = studentArrayAdapter


    }
}