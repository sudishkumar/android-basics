package com.famouscoader.firsttech.activities

import android.content.Intent
import android.os.Bundle
import android.widget.EditText
import android.widget.ListView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import androidx.fragment.app.Fragment
import com.famouscoader.firsttech.R
import com.famouscoader.firsttech.adapters.CustomAdaptor
import com.famouscoader.firsttech.fragment.HomeFragment
import com.famouscoader.firsttech.fragment.ProfileFragment
import com.famouscoader.firsttech.fragment.SettingsFragment
import com.famouscoader.firsttech.models.CustomListModel
import com.famouscoader.firsttech.objects.Global.customDataList
import com.google.android.material.bottomnavigation.BottomNavigationView

/*

1. For custom list we need model (data class)
Like this
    data class CustomListModel(
    val name: String? = null,
    val imageURL: String? = null,
    val email: String? = null
   )

2. we need a adapter ( CustomAdaptor.kt ) for rendering data and view to listView on (activity/ fragment) xml
3. We need an extra xml layout for list item design (custom_list_item.xml)
4. We need data for sending in adapter to add in design
5. Our data should be look like this ( val customDataList = ArrayList<CustomListModel>())
6. We will add data into array like this (  customDataList.add(CustomListModel(name = "Sk", imageURL = "", email = "Sk@gmail.com")) )
7. After creating adapter we will set adapter in listView
Like this
        val cusListView = findViewById<ListView>(R.id.cusList)
        cusListView.adapter = CustomAdaptor(customDataList, this)

 */



class MainActivity : AppCompatActivity() {
    lateinit var addStudent:AppCompatButton


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        customDataList.add(CustomListModel(name = "Sk", imageURL = "https://quoteshindi.net/wp-content/uploads/different-good-morning-images.jpg", email = "Sk@gmail.com"))
        customDataList.add(CustomListModel(name = "Sks", imageURL = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSVGHL9r9OucwArH8yO3rEDPryG4V3tSCBw-w&usqp=CAU", email = "Sks@gmail.com"))
        customDataList.add(CustomListModel(name = "Sk1", imageURL = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSVGHL9r9OucwArH8yO3rEDPryG4V3tSCBw-w&usqp=CAU", email = "Sk1@gmail.com"))
        customDataList.add(CustomListModel(name = "Sk2", imageURL = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSVGHL9r9OucwArH8yO3rEDPryG4V3tSCBw-w&usqp=CAU", email = "Sk2@gmail.com"))
        customDataList.add(CustomListModel(name = "Sk3", imageURL = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSVGHL9r9OucwArH8yO3rEDPryG4V3tSCBw-w&usqp=CAU", email = "Sk3@gmail.com"))
        customDataList.add(CustomListModel(name = "Sk4", imageURL = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSVGHL9r9OucwArH8yO3rEDPryG4V3tSCBw-w&usqp=CAU", email = "Sk4@gmail.com"))
        customDataList.add(CustomListModel(name = "Sk5", imageURL = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSVGHL9r9OucwArH8yO3rEDPryG4V3tSCBw-w&usqp=CAU", email = "Sk5@gmail.com"))

        val cusListView = findViewById<ListView>(R.id.cusList)
        cusListView.adapter = CustomAdaptor(customDataList, this)

        val  bottomNav = findViewById<BottomNavigationView>(R.id.bottomNav)
        loadFragment(HomeFragment())
        bottomNav.setOnItemSelectedListener {
            when (it.itemId) {
                R.id.settings -> {
                    loadFragment(SettingsFragment())
                }
                R.id.profile -> {
                    loadFragment(ProfileFragment())

                }
                else -> {
                    loadFragment(HomeFragment())
                }
            }
             true
        }


        val name = findViewById<EditText>(R.id.name_editText)
        val email = findViewById<EditText>(R.id.email_editText)
       addStudent = findViewById(R.id.addStudent_appCompatButton)
        addStudent.setOnClickListener {
            val i = Intent(this, StudentListActivity::class.java)
            i.putExtra("name", name.text.toString())
            i.putExtra("email", email.text.toString())
            startActivity(i)
        }
    }
    private  fun loadFragment(fragment: Fragment){
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container,fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    override fun onStart() {
        super.onStart()
        Toast.makeText(this, "onStart", Toast.LENGTH_SHORT).show()
    }

    override fun onResume() {
        super.onResume()
        Toast.makeText(this, "onResume", Toast.LENGTH_SHORT).show()



    }

    override fun onRestart() {
        super.onRestart()
        Toast.makeText(this, "onRestart", Toast.LENGTH_SHORT).show()

    }

    override fun onStop() {
        super.onStop()
        Toast.makeText(this, "onStop", Toast.LENGTH_SHORT).show()
    }

    override fun onPause() {
        super.onPause()
        Toast.makeText(this, "onPause", Toast.LENGTH_SHORT).show()
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }
}