package com.famouscoader.firsttech.objects

import com.famouscoader.firsttech.models.CustomListModel

object Global {
    val studentDetailsArray = arrayListOf("Abhi abhi@gmail.com", "amit, amit@gmail.com", "Sonu, sonu@gmail.com")
    val customDataList = ArrayList<CustomListModel>()

}