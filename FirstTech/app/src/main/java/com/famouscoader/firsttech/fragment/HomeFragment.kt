package com.famouscoader.firsttech.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import com.famouscoader.firsttech.R
import com.famouscoader.firsttech.adapters.CustomAdaptor
import com.famouscoader.firsttech.models.CustomListModel

class HomeFragment : Fragment() {
    val customDataList = ArrayList<CustomListModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var rowView =inflater.inflate(R.layout.fragment_home, container, false)

        customDataList.add(CustomListModel(name = "Sk", imageURL = "", email = "Sk@gmail.com"))
        customDataList.add(CustomListModel(name = "Sks", imageURL = "", email = "Sks@gmail.com"))
        customDataList.add(CustomListModel(name = "Sk1", imageURL = "", email = "Sk1@gmail.com"))
        customDataList.add(CustomListModel(name = "Sk2", imageURL = "", email = "Sk2@gmail.com"))
        customDataList.add(CustomListModel(name = "Sk3", imageURL = "", email = "Sk3@gmail.com"))
        customDataList.add(CustomListModel(name = "Sk4", imageURL = "", email = "Sk4@gmail.com"))
        customDataList.add(CustomListModel(name = "Sk5", imageURL = "", email = "Sk5@gmail.com"))

        val cusListView = rowView.findViewById<ListView>(R.id.cusList)
        cusListView.adapter = activity?.let { CustomAdaptor(customDataList, it) }

        return  rowView
    }
}