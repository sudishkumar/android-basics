package com.famouscoader.firsttech.models

data class CustomListModel(
    val name: String? = null,
    val imageURL: String? = null,
    val email: String? = null
)
